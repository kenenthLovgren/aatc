﻿using System;

namespace BoxingGame
{

    class Boxer
    {
        public string Name { get; set; }
        public int Health { get; set; }
        public int Strength { get; set; }
        public int Stamina { get; set; }
        public int Victories { get; set; }
    }

    public class Ability
    {
        public string AbilityType { get; set; }
        public int Dmg { get; set; }
        public bool OffCooldown { get; set; }
        public int CooldownTimer { get; set; }
        public int DotDmg { get; set; }
        public int DotTimer { get; set; }

    }

    class BoxingGame
    {

        static void Main(string[] args)
        {
            InitializePlayers();
        }

        public static void InitializePlayers()
        {
            //player instanziation 
            var player1 = new Boxer();
            var player2 = new Boxer();

            var player1Ability = new Ability();
            var player2Ability = new Ability();


            player1.Name = "Tyson";
            player1.Health = 20;
            player1.Strength = 10;
            player1.Victories = 0;
            player1.Stamina = 2;

            player1Ability.AbilityType = "Bite";
            player1Ability.Dmg = 20;
            player1Ability.OffCooldown = false;
            player1Ability.CooldownTimer = 5;
            player1Ability.DotDmg = 1;
            player1Ability.DotTimer = 0;

            player2.Name = "Brian";
            player2.Health = 200;
            player2.Strength = 5;
            player2.Victories = 0;
            player2.Stamina = 10;

            player2Ability.AbilityType = "Regen";
            player2Ability.Dmg = 20;
            player2Ability.OffCooldown = false;
            player2Ability.CooldownTimer = 7;
            player2Ability.DotDmg = 0;


            Random rnd = new Random();

            try
            {
                Console.WriteLine("How many rounds do you want to fight");

                int matches;
                matches = Convert.ToInt32(Console.ReadLine());

                for (int BoxingSimmatches = 1; BoxingSimmatches <= matches; BoxingSimmatches++)
                {

                    for (int round = 0; round < 3; round++)
                    {
                        for (int i = 0; i < 20; i++)
                        {
                            if (player1.Health <= 0 || player2.Health <= 0)
                            {
                                break;
                            }

                            Console.WriteLine();

                            Console.WriteLine("Press = W = for a normal Attack");
                            Console.WriteLine("Press = D = for a Special Attack");
                            Console.WriteLine("Ability Cooldowns:\n{0}'s ablility {1} has a cooldown of {2}", player1.Name, player1Ability.AbilityType, player1Ability.CooldownTimer);
                            Console.WriteLine("{0}'s ablility {1} has a cooldown of {2}", player2.Name, player2Ability.AbilityType, player2Ability.CooldownTimer);
                            Console.WriteLine("Which attack do you want to perform");


                            var input = Console.ReadLine();

                            switch (input)
                            {
                                case "w":

                                    var dmg1 = rnd.Next(player1.Strength + 4);
                                    var dmg2 = rnd.Next(player2.Strength + 1);
                                    var regen1 = rnd.Next(player1.Stamina);
                                    var regen2 = rnd.Next(player2.Stamina);

                                    //player 1 Attack
                                    if (player1Ability.OffCooldown)
                                    {
                                        Console.WriteLine("{0} Uses his Ability-Attack {1}, and deals {2} dmg to {3}, {3} now has {4} health", player1.Name, player1Ability.AbilityType, player1Ability.Dmg, player2.Name, player2.Health -= player1Ability.Dmg);
                                        Console.WriteLine("{0} applies {1} of bleed dmg to {2} for 3 rounds", player1.Name, player1Ability.DotDmg, player2.Name);

                                        player1Ability.DotTimer = 3;
                                        player1Ability.OffCooldown = false;
                                        player1Ability.CooldownTimer = 6;
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0} Hits {1} for {2}, {1} takes {2} Damage, {1} now has {3} health left", player1.Name, player2.Name, dmg1, player2.Health -= dmg1);
                                        if (player1Ability.DotTimer > 0)
                                        {
                                            Console.WriteLine("{0} takes {1} bleed dmg. {0} now has {2} health", player2.Name, player1Ability.DotDmg, player2.Health -= 1);
                                            player1Ability.DotTimer--;
                                        }
                                    }

                                    //player 2 attack
                                    Console.WriteLine("{0} Hits {1} for {2}, {1} takes {2} Damage, {1} now has {3} health left", player2.Name, player1.Name, dmg2, player1.Health -= dmg2);


                                    Console.WriteLine("");

                                    Console.WriteLine("{0} Regains some of his stamina back and recives {1} health, now {0} has {2} health", player1.Name, regen1, regen1 += player1.Health);

                                    if (player2Ability.OffCooldown)
                                    {
                                        Console.WriteLine("{0} Uses his Ability-Attack {1}, and regens for {2} health, now {0} has {3} health", player2.Name, player2Ability.AbilityType, player2Ability.Dmg, player2.Health += player2Ability.Dmg);
                                        player2Ability.CooldownTimer = 8;
                                        player2Ability.OffCooldown = false;
                                    }
                                    Console.WriteLine("{0} Regains some of his stamina back and recives {1} health, now {0} has {2} health", player2.Name, regen2, regen2 += player2.Health);
                                    break;


                                case "d":
                                    dmg1 = rnd.Next(player1.Strength + 12);
                                    dmg2 = rnd.Next(player2.Strength + 1);
                                    regen1 = rnd.Next(player1.Stamina);
                                    regen2 = rnd.Next(player2.Stamina);


                                    //player 1 Attack
                                    if (player1Ability.OffCooldown)
                                    {
                                        Console.WriteLine("{0} Uses his Ability-Attack {1}, and deals {2} dmg to {3}, {3} now has {4} health", player1.Name, player1Ability.AbilityType, player1Ability.Dmg, player2.Name, player2.Health -= player1Ability.Dmg);
                                        Console.WriteLine("{0} applies {1} of bleed dmg to {2} for {3} rounds", player1.Name, player1Ability.DotDmg, player2.Name, player1Ability.CooldownTimer);
                                        Console.WriteLine("{0} takes {1} bleed dmg. {0} now has {2} health", player2.Name, player1Ability.DotDmg, player2.Health -= 1);

                                        player1Ability.CooldownTimer = 6;
                                        player1Ability.OffCooldown = false;
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0} Hits {1} for {2}, {1} takes {2} Damage, {1} now has {3} health left", player1.Name, player2.Name, dmg1, player2.Health -= dmg1);
                                        if (player1Ability.DotTimer > 0)
                                        {
                                            Console.WriteLine("{0} takes {1} bleed dmg. {0} now has {2} health", player2.Name, player1Ability.DotDmg, player2.Health -= 1);
                                            player1Ability.DotTimer--;

                                        }
                                    }

                                    Console.WriteLine("{0} Hits {1} for {2}, {1} takes {2} Damage, {1} now has {3} health left", player2.Name, player1.Name, dmg2, player1.Health -= dmg2);
                                    Console.WriteLine("");
                                    Console.WriteLine("{0} Regains some of his stamina back and recives {1} health, now {0} has {2} health", player1.Name, regen1, regen1 += player1.Health);

                                    if (player2Ability.OffCooldown)
                                    {
                                        Console.WriteLine("{0} Uses his Ability-Attack {1}, and regens for {2} health, now {0} has {3} health", player2.Name, player2Ability.AbilityType, player2Ability.Dmg, player2.Health += player2Ability.Dmg);
                                        player2Ability.CooldownTimer = 8;
                                        player2Ability.OffCooldown = false;

                                    }
                                    else
                                    {
                                        Console.WriteLine("{0} Regains some of his stamina back and recives {1} health, now {0} has {2} health", player2.Name, regen2, regen2 += player2.Health);
                                    }


                                    break;
                                case "color":
                                    Console.BackgroundColor = ConsoleColor.Cyan;
                                    Console.ForegroundColor = ConsoleColor.Black;
                                    break;

                                default:
                                    Console.WriteLine("It was not the keys you where told to press");
                                    break;

                            }
                            player1Ability.CooldownTimer -= 1;
                            player2Ability.CooldownTimer -= 1;

                            if (player1Ability.CooldownTimer == 0)
                            {
                                player1Ability.OffCooldown = true;
                            }

                            if (player2Ability.CooldownTimer == 0)
                            {
                                player2Ability.OffCooldown = true;
                            }



                        }

                        if (player2.Health <= 0)
                        {
                            Console.WriteLine("");
                            player1.Victories++;
                            Console.WriteLine();

                            Console.WriteLine(player1.Name + " knocked down " + player2.Name + ". " + player1.Name + " total number of knock downs " + player1.Victories);

                            Console.WriteLine("=========================================================================");
                            Console.WriteLine("############################## {0} Wins the round ##################################", player1.Name);
                            Console.WriteLine("=========================================================================");
                            Console.WriteLine();

                            player1.Health = 20;
                            player2.Health = 200;

                            player1Ability.CooldownTimer = 5;
                            player2Ability.CooldownTimer = 7;
                            player1Ability.DotTimer = 0;



                        }
                        else if (player1.Health <= 0)
                        {
                            Console.WriteLine("");
                            player2.Victories++;
                            Console.WriteLine(player2.Name + " knocked down " + player1.Name + ". " + player2.Name + " total number of knock downs " + player2.Victories);
                            Console.WriteLine("=========================================================================");
                            Console.WriteLine("############################## {0} Wins the round ##################################", player2.Name);
                            Console.WriteLine("=========================================================================");
                            Console.WriteLine();

                            player1.Health = 20;
                            player2.Health = 200;

                            player1Ability.CooldownTimer = 5;
                            player2Ability.CooldownTimer = 7;
                            player1Ability.DotTimer = 0;

                        }


                        if (player1.Victories >= matches)
                        {
                            Console.WriteLine();
                            Console.WriteLine("=========================================================================");
                            Console.WriteLine("*_ **_= ***_===****|| {0} is the Champion ****===_***==_**_*", player1.Name);
                            Console.WriteLine("=========================================================================");
                            Console.WriteLine();

                            player1Ability.CooldownTimer = 5;
                            player2Ability.CooldownTimer = 7;
                            player1Ability.DotTimer = 0;

                            break;
                        }
                        else if (player2.Victories >= matches)
                        {
                            Console.WriteLine();
                            Console.WriteLine("=========================================================================");
                            Console.WriteLine("*_ **_= ***_===****|| {0} is the Champion ****===_***==_**_*", player2.Name);
                            Console.WriteLine("=========================================================================");
                            Console.WriteLine();

                            player1Ability.CooldownTimer = 5;
                            player2Ability.CooldownTimer = 7;
                            player1Ability.DotTimer = 0;


                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {

                Console.WriteLine("you Must enter the number of rounds you want to fight");
                Console.ReadLine();
                InitializePlayers();

            }
        }
    }
}
