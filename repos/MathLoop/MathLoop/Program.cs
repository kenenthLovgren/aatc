﻿using System;

namespace MathLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            // 0.3 mm thick paper
            double number = 0.3;

            var times = 100;

            double efielTowerTheory = 0;

            for (int i = 1; i <= times; i++)
            {

                if (efielTowerTheory <= 10)
                {
                    efielTowerTheory = number *= 2;
                    Console.WriteLine($"The peice of paper is now {Math.Round(efielTowerTheory, 1)} mm tall. The peice of paper is now folded {i} times");

                }
                else if (efielTowerTheory >= 10 && efielTowerTheory <= 100)
                {
                    efielTowerTheory = number *= 2;
                    Console.WriteLine($"The peice of paper is now { Math.Round(efielTowerTheory / 10, 2)} cm tall. The peice of paper is now folded {i} times");

                }

                if(efielTowerTheory >= 100 && efielTowerTheory <= 100000)
                {
                    efielTowerTheory = number *= 2;
                    Console.WriteLine($"The peice of paper is now {Math.Round(efielTowerTheory / 1000, 2)} meters tall. The peice of paper is now folded {i} times");
                }
                else if (efielTowerTheory >= 100000 && efielTowerTheory <= 1000000000000)
                {
                    efielTowerTheory = number *= 2;
                    Console.WriteLine($"The peice of paper is now {Math.Round(efielTowerTheory, 2)} km's tall. The peice of paper is now folded {i} times");
                }

                else if (efielTowerTheory >= 1000000000000 && efielTowerTheory <= 1000000000000000000)
                {
                    efielTowerTheory = number *= 2;
                    Console.WriteLine($"The peice of paper is now { Math.Round(efielTowerTheory, 1)} lightYears Away. The peice of paper is now folded {i} times");
                }

            }


        }
    }
}
