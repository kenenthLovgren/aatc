﻿using System;
using System.Windows;

namespace FoldingPaperApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            metaLbl.Content = ($"This is an app that can, calculate how tall a paper would be if folding in half X times");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            int times = Convert.ToInt32(TxtBoxLblTimes.Text);

            System.Windows.Controls.TextBox result = txtBoxResult;

            double math = Convert.ToDouble(TxtBoxfold.Text);

            double multiply = math;

            for (int i = 0; i < times; i++)
            {

                if (multiply <= 10)
                {
                    multiply *= 2;
                    result.Text = $"The peice of paper is now {multiply} mm tall. The peice of paper is now folded {times} times";

                }
                else if (multiply >= 10 && multiply <= 100)
                {
                    multiply *= 2;
                    result.Text = $"The peice of paper is now {Math.Round(multiply / 10)} cm tall. The peice of paper is now folded {times} times";
                    
                }

                else if (multiply >= 100 && multiply <= 100000)
                {
                    multiply *= 2;
                    result.Text = $"The peice of paper is now {multiply / 1000} meters tall. The peice of paper is now folded {times} times";
                }

                else if (multiply >= 100000 && multiply <= 100000000000)
                {
                    multiply *= 2;
                    result.Text = $"The peice of paper is now {Math.Round(multiply / 1000000, 1)} km's tall. The peice of paper is now folded {times} times";
                }

                else if (multiply >= 100000000000 && multiply <= 1000000000000000)
                {
                    multiply *= 2;
                    result.Text = $"The peice of paper is now {Math.Round(multiply / 10000000000000000000, 5)} Lightyears tall. The peice of paper is now folded {times} times, wow thats far away";
                }

                else if (multiply >= 1000000000000000)
                {
                    result.Text = "we havent expolored, so far out yet.";
                }

            }


        }
    }
}
